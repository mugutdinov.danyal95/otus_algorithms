#pragma once
#include <filesystem>
#include <fstream>
#include <iostream>
#include <optional>
#include <regex>
#include <string>

struct cmp {
    bool operator()(const std::filesystem::path &lhs, const std::filesystem::path &rhs) const {

        if (lhs.string().size() == rhs.string().size())
            return lhs < rhs;
        else
            return lhs.string().size() < rhs.string().size();
    }
};

std::optional<uint16_t> get_number_if_test_in(const std::string &filename) {
    std::smatch m;
    std::regex e("test.([0-9]+).in");

    if (std::regex_search(filename, m, e)) {
        return {std::stoi(m[1])};
    }

    return {};
}

std::string get_file_content(const std::filesystem::path &file) {
    std::ifstream t(file.string());

    return std::string((std::istreambuf_iterator<char>(t)),
                       std::istreambuf_iterator<char>());
}

namespace fs = std::filesystem;
class Tester {
    std::string _path;

    bool check_exist_out(uint16_t n) {
        return std::filesystem::exists(_path + "test." + std::to_string(n) + ".out");
    }

    std::map<std::filesystem::path, std::filesystem::path, cmp> _in_out_path_pairs;

public:
    Tester(std::string_view directory) : _path(directory) {
        make_in_out_paths();
    }
    void
    make_in_out_paths() {
        for (const auto &entry: fs::directory_iterator(_path)) {

            if (auto n = get_number_if_test_in(entry.path().string())) {
                std::filesystem::path out{_path + "test." + std::to_string(*n) + ".out"};
                if (not check_exist_out(*n)) {
                    std::cout << "for test." << *n << ".in"
                              << " there is no test." << *n << ".out file" << std::endl;
                }

                _in_out_path_pairs.emplace(entry.path(), out);
            }
        }
    }

    void verify(const std::string &name, const std::function<long(long)> &&f, uint16_t limit = 0) {

        std::cout << "Test for: " << name << std::endl;

        uint16_t test_number = 1;
        for (auto &&it: _in_out_path_pairs) {
            if (limit != 0 and test_number > limit)
                return;
            test_number++;

            auto in = get_file_content(it.first);
            auto out = get_file_content(it.second);

            auto result = f(std::stol(in));
            auto expected = std::stol(out);
            if (result == expected) {
                std::cout << it.first.filename() << " | " << it.second.filename() << " ok" << std::endl;
            } else {
                std::cout << it.first.filename() << " | " << it.second.filename() << " failed" << std::endl;
                std::cout << "expected: " << expected << " result: " << result << std::endl;
            }
        }
    }
};