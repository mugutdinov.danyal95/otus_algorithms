#include "algorithms.h"
#include "tester.h"
#include <iostream>

int main(int argc, char *argv[]) {

    if (argc < 2) {
        std::cerr << "need path to test suites" << std::endl;
        return -1;
    }

    std::cout << "simplest: " << simplest() << std::endl;
    std::cout << "optimize1: " << optimize1() << std::endl;
    std::cout << "optimize2: " << optimize2() << std::endl;
    std::cout << "optimize3: " << optimize3() << std::endl;

    Tester tester{argv[1]};

    tester.verify(
            "CommonWay", [](long n) -> long {
                CommonWay::count = 0;
                CommonWay::get_lucky_general(n, 0, 0);
                return CommonWay::count;
            },
            3);


    tester.verify(
            "CommonWayOptimize1", [](long n) -> long {
                CommonWayOptimize1::count = 0;
                CommonWayOptimize1::get_lucky_general(n, 0, 0);
                return CommonWayOptimize1::count;
            },
            3);

    tester.verify("common_best", common_best);
}
