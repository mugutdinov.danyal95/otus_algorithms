#include "algorithms.h"
#include <vector>

std::size_t simplest() {

    std::size_t count = 0;
    for (std::size_t i1 = 0; i1 < 10; i1++)
        for (std::size_t i2 = 0; i2 < 10; i2++)
            for (std::size_t i3 = 0; i3 < 10; i3++)
                for (std::size_t b1 = 0; b1 < 10; b1++)
                    for (std::size_t b2 = 0; b2 < 10; b2++)
                        for (std::size_t b3 = 0; b3 < 10; b3++) {
                            auto sumI = i1 + i2 + i3;
                            auto sumB = b1 + b2 + b3;
                            if (sumI == sumB) {
                                count++;
                            }
                        }

    return count;
}

std::size_t optimize1() {

    std::size_t count = 0;
    for (std::size_t i1 = 0; i1 < 10; i1++)
        for (std::size_t i2 = 0; i2 < 10; i2++)
            for (std::size_t i3 = 0; i3 < 10; i3++) {
                auto sumI = i1 + i2 + i3;
                for (std::size_t b1 = 0; b1 < 10; b1++)
                    for (std::size_t b2 = 0; b2 < 10; b2++)
                        for (std::size_t b3 = 0; b3 < 10; b3++) {
                            auto sumB = b1 + b2 + b3;
                            if (sumI == sumB) {
                                count++;
                            }
                        }
            }

    return count;
}

std::size_t optimize2() {

    std::size_t count = 0;
    for (std::size_t i1 = 0; i1 < 10; i1++)
        for (std::size_t i2 = 0; i2 < 10; i2++)
            for (std::size_t i3 = 0; i3 < 10; i3++) {
                auto sumI = i1 + i2 + i3;
                for (std::size_t b1 = 0; b1 < 10; b1++)
                    for (std::size_t b2 = 0; b2 < 10; b2++) {
                        auto sumB = b1 + b2;
                        if ((sumI >= sumB) and (sumI - sumB < 10)) {
                            count++;
                        }
                    }
            }

    return count;
}

std::size_t optimize3() {

    std::size_t count = 0;
    for (std::size_t i1 = 0; i1 < 10; i1++)
        for (std::size_t i2 = 0; i2 < 10; i2++) {
            long sumI = i1 + i2;
            for (std::size_t b1 = 0; b1 < 10; b1++)
                for (std::size_t b2 = 0; b2 < 10; b2++) {
                    long sumB = b1 + b2;

                    auto abs = std::abs(sumI - sumB);
                    if (abs < 10) {
                        count += 10 - abs;
                    }
                }
        }
    return count;
}

void print(std::vector<std::vector<uint64_t>> &table) {
    for (auto &it1: table) {
        for (auto &it2: it1)
            std::cout << it2 << '\t';
        std::cout << std::endl;
    }
}


void to_zero(std::vector<uint64_t> &vec) {
    for (auto &it2: vec) {
        it2 = 0;
    }
}

void resize_table_for_n(std::vector<std::vector<uint64_t>> &vec, int n) {
    int column_n = (n * 9) + 1;

    for (auto &it: vec) {
        it.resize(column_n);
        to_zero(it);
    }
}


void set_to_table_with_offset(std::vector<std::vector<uint64_t>> &table, const std::vector<uint64_t> &result_row) {
    for (int i = 0; i < table.size(); i++) {
        for (int j = 0; j < result_row.size(); j++) {
            table[i][i + j] = result_row[j];
        }
    }
}

void calculate_result_row(std::vector<std::vector<uint64_t>> &table, std::vector<uint64_t> &result_row) {
    for (int j = 0; j < result_row.size(); j++) {
        uint64_t count = 0;
        for (int i = 0; i < table.size(); i++) {
            count += table[i][j];
        }
        result_row[j] = count;
    }
}

uint64_t calc_result(const std::vector<uint64_t> &result_row) {
    uint64_t result = 0;
    for (auto i: result_row) {
        result += i * i;
    }
    return result;
}

uint64_t common_best(std::size_t n) {

    std::vector<std::vector<uint64_t>> table(10);

    std::vector<uint64_t> result_row{1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

    for (int current_n = 2; current_n <= n; current_n++) {
        resize_table_for_n(table, current_n);
        set_to_table_with_offset(table, result_row);
        result_row.resize((current_n * 9) + 1);//+1 из-за нуля
        calculate_result_row(table, result_row);
    }
    return calc_result(result_row);
}
