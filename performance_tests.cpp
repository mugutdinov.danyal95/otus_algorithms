#include <benchmark/benchmark.h>
#include <iostream>
#include "algorithms.h"

static void simplest(benchmark::State &state) {
    for (auto _: state) {
        simplest();
    }
}
BENCHMARK(simplest);

static void optimize1(benchmark::State &state) {
    for (auto _: state) {
        optimize1();
    }
}
BENCHMARK(optimize1);

static void optimize2(benchmark::State &state) {
    for (auto _: state) {
        optimize2();
    }
}
BENCHMARK(optimize2);

static void optimize3(benchmark::State &state) {

    for (auto _: state) {
        optimize3();
    }
}
BENCHMARK(optimize3);


static void generale(benchmark::State &state) {
    CommonWay::count = 0;
    for (auto _: state) {
        CommonWay::get_lucky_general(3, 0, 0);
    }
}
BENCHMARK(generale);

static void generaleOptimize1(benchmark::State &state) {
    CommonWayOptimize1::count = 0;

    for (auto _: state) {
        CommonWayOptimize1::get_lucky_general(3, 0, 0);
    }
}
BENCHMARK(generaleOptimize1);

static void best(benchmark::State &state) {
    CommonWayOptimize1::count = 0;

    for (auto _: state) {
        common_best(5);
    }
}
BENCHMARK(best);


BENCHMARK_MAIN();
