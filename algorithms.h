#pragma once

#include <cstring>
#include <iostream>


std::size_t simplest();

std::size_t optimize1();

std::size_t optimize2();

std::size_t optimize3();

struct CommonWay {
    inline static long count = 0;
    static void get_lucky_general(long n, long sumA, long sumB) {
        if (n == 0) {
            if (sumA == sumB) {
                count++;
            }
            return;
        }
        for (long a = 0; a < 10; a++)
            for (long b = 0; b < 10; b++) {
                get_lucky_general(n - 1, sumA + a, sumB + b);
            }
    }
};

struct CommonWayOptimize1 {
    inline static long count = 0;
    static void get_lucky_general(long n, long sumA, long sumB) {
        if (n == 1) {
            auto abs = std::abs(sumA - sumB);
            if (abs < 10) {
                count += 10 - abs;
            }
            return;
        }
        for (long a = 0; a < 10; a++)
            for (long b = 0; b < 10; b++) {
                get_lucky_general(n - 1, sumA + a, sumB + b);
            }
    }
};

uint64_t common_best(std::size_t n);
